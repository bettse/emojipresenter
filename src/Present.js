import React from 'react';

import { Emoji } from 'emoji-mart'
import 'emoji-mart/css/emoji-mart.css'

import { useParams } from "react-router-dom";

import customEmojis from './customEmojis'
import useBodyClass from './useBodyClass'

const fallback = (emoji, props) => {
  if (emoji) {
    return `:${emoji.short_names[0]}:`
  }

  const custom = customEmojis.find(({short_names}) => short_names.includes(props.emoji))
  return (
    <img height={props.size} width={props.size} src={custom.imageUrl} alt={custom.name} />
  );
}

function Present() {
  useBodyClass('present');

  let { emojiId } = useParams();

  return (
    <Emoji
      native
      size={512}
      emoji={emojiId}
      custom={customEmojis}
      fallback={fallback}
    />
  )
}

export default Present;
