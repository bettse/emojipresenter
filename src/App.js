import React from 'react';

import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import Container from 'react-bootstrap/Container'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'

import Select from './Select'
import Present from './Present'

import './App.css'

function App() {
  return (
    <Container fluid>
      <Row noGutters className="justify-content-center pt-lg-5 mt-lg-5 mx-auto">
        <Col xs="auto" sm="auto" md="auto" lg="auto" xl="auto">
          <Router>
            <Switch>
              <Route path="/:emojiId">
                <Present />
              </Route>
              <Route path="/">
                <Select />
              </Route>
            </Switch>
          </Router>
        </Col>
      </Row>
    </Container>
  );
}

export default App;
