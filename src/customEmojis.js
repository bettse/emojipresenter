const customEmojis = [{
  name: 'Octocat',
  short_names: ['octocat'],
  text: '',
  emoticons: [],
  keywords: ['github'],
  imageUrl: '/assets/octocat.png',
},{
  name: 'Thank you heart',
  short_names: ['ty_heart'],
  text: '',
  emoticons: [],
  keywords: ['ty', 'heart'],
  imageUrl: '/assets/ty_heart.png',
},{
  name: 'Netlify Logo',
  short_names: ['netlify'],
  text: '',
  emoticons: [],
  keywords: ['netlify', 'netflix'],
  imageUrl: '/assets/netlify.png',
}]

export default customEmojis
