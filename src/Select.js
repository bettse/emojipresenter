import React, { useState } from 'react';

import { Picker, emojiIndex } from 'emoji-mart'
import 'emoji-mart/css/emoji-mart.css'

import { isMobileOnly } from "react-device-detect";

import customEmojis from './customEmojis'

const { URL = 'https://emoji.ericbetts.dev/' } = process.env;
const TOHORA_URL = "https://dashboard.ericbetts.org/launch/"

const recent = [
  'large_green_square',
  '+1',
  'ty_heart',
  'netlify',
  'thinking_face', 'hand',
  'one', 'two', 'three', 'five', 'eight',
  'alarm_clock',
  'heart', 'mask', 'coffee',
  'slightly_smiling_face'
];

function Select() {
  const [lastUpdate, setLastUpdate] = useState("");
  const [status, setStatus] = useState("");

  const pickEmoji = async (emoji) => {
    const { id } = emoji;
    const body = new URLSearchParams();
    body.set('text', `${URL}${id}`)

    const response = await fetch(TOHORA_URL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body,
    })
    setLastUpdate(await response.text())
    setStatus(response.ok ? "large_green_circle" : "red_circle")
  }

  const title = lastUpdate.replace(/:(\w+):/gi, (match, p1) => {
    return emojiIndex.search(p1).map(o => o.native)[0];
  })

  return (
    <>
      <Picker
        native
        autoFocus
        enableFrequentEmojiSort
        showPreview={!isMobileOnly}
        recent={recent}
        custom={customEmojis}
        title={title}
        emoji={status}
        showSkinTones={false}
        emojiSize={48}
        perLine={12}
        onSelect={pickEmoji}
        theme="auto"
      />
    </>
  );
}


export default Select;
